<html>
<head>
	<style>
		ul {
			 background: #ff9999;
			 padding: 20px;
		   }
		
		ul li {
    			    background: #ffe5e5;
			    padding: 5px;
			    margin-left: 35px;
		      }

	</style>
</head>
<body>
<?php

require_once '../lib/movieInfoManager/movieInfoManager.php';
require_once '../lib/movieInfoManager/ratingParams.php';
require_once '../third-party/IMDb-PHP-API/class_IMDb.php';
require_once '../third-party/imdbphpapi/imdb.class.php';

try{

	if($_SERVER['REQUEST_METHOD']=='POST')
	{

		$genre = $_POST['genre'];
		$movieInfoManager = new movieInfoManager();
		$to250Movies = $movieInfoManager->getTopMovies();
		$top50movies = $movieInfoManager->selectTopFiftyMovies($to250Movies, $genre); 
		echo "<h2>Here are the list of recommended movies</h2><br>";
		echo "<ul>";
		foreach($top50movies as $movie)
		{
			echo "<li>".$movie['ratingParams']->getTitle()."</li>";
		}
		echo "</ul>";
	}

}catch(Exception $ex)
{
	echo "<h2>Opps!! Something went wrong. Please try again later.</h2>";die;
}
?>
</body>
</html>
