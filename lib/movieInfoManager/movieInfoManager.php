<?php

class movieInfoManager
{

	public function getTopMovies()
	{
		try{
			$imdb = new IMDbMovies(true, true, 0);
			$movies = $imdb->chart_top();
			$ratingParamsList=  [];
			for( $i=0 ; $i<150 ; $i++)
			{
				$tconst = $movies[$i]->tconst;
				$tconst=substr($tconst,2);

				$moviesInfo = new imdb($tconst);
				$ratingParams = new ratingParams(); 
				$ratingParams->setRating($movies[$i]->rating);
				$ratingParams->setTitle($movies[$i]->title);
				$ratingParams->setYearOfMovie($movies[$i]->year);
				$ratingParams->setVotes($moviesInfo->votes());
				$ratingParams->setMovieRecommendations($moviesInfo->movie_recommendations());
				$ratingParams->setGenre($moviesInfo->genre());
				$ratingParamsList[$tconst]['ratingParams']= $ratingParams;
				$ratingParamsList[$tconst]['tConst']=$tconst;
			}

			return $ratingParamsList;
		}catch(Exception $ex)
		{
			return $ex->getMessage();
		}
	}

	public function selectTopFiftyMovies($totalMovieList, $genre) {

		try{
			$selectedMovie =[];
			foreach ($totalMovieList as $movieInfo)
			{
				if($genre[$movieInfo['ratingParams']->getGenre()])
				{
					$selectedMovie[$movieInfo['tConst']]= $movieInfo;
				}
			}      
			$selectedMovie = $this->addMoviewFromRecommendedList($totalMovieList, $selectedMovie);
			$selectedMovie = $this->addRemainingMovie($totalMovieList , $selectedMovie);
			return $selectedMovie;

		}catch(Exception $ex)
		{
			return $ex->getMessage();
		}
	}

	private function addMoviewFromRecommendedList($totalMovieList, $selectedMovie)
	{
		$numMoviesInList = sizeof($selectedMovie);
		$numMoviewToBeAdded = 50 - $numMoviesInList;
		$count=0;
		$key = 0;
		foreach($selectedMovie as $topMovie)
		{
			$recommendedMovis = $topMovie['ratingParams']->getMovieRecommendations();
			foreach($recommendedMovis as $movie)
			{                            
				$tConst = $movie['imdbid'];
				if($totalMovieList[$tConst] && $count<=$numMoviewToBeAdded && !$selectedMovie[$tConst])
				{
					$selectedMovie[$tConst] = $totalMovieList[$tConst];
					$count++;
				}
				else if($count>=$numMoviewToBeAdded) 
				{
					break;
				}
			}
			if($count>=$numMoviewToBeAdded)
			{
				break;
			}
			$key++;
		}
		return $selectedMovie;
	}

	private function addRemainingMovie($totalMovieList,$selectedMovie)
	{
		$numMoviesInList = sizeof($selectedMovie);
                $numMoviewToBeAdded = 50 - $numMoviesInList;
                $count=0;
		foreach($totalMovieList as $addMovie)
		{
			$tConst = $addMovie['tConst'];
			if(!$selectedMovie[$tConst] && $count<$numMoviewToBeAdded)
			{
				$selectedMovie[$tConst] = $totalMovieList[$tConst];
				$count++;
			}		
			if($numMoviewToBeAdded<=count)
			{
				break;
			}
		}
		return $selectedMovie;
			
	}
}
