<?php

class ratingParams
{
	private $rating;
        private $title;
        private $yearOfMovie;
	private $votes;
	private $movie_recommendations;
	private $genre;
	
        function getRating()
        {
                return $this->rating;
        }
        function setRating($rating)
        {
                $this->rating = $rating;
        }
        
        function getTitle()
        {
                return $this->title;
        }
        function setTitle($title)
        {
                $this->title = $title;
        }
	 
	function getYearOfMovie()
        {
                return $this->yearOfMovie;
        }
        function setYearOfMovie($year)
        {
                $this->yearOfMovie = $year;
        }

	function getMovieRecommendations()
	{
		return $this->movie_recommendations;
	}

	function setMovieRecommendations($movieRecommendations)
	{
		$this->movie_recommendations = $movieRecommendations;
	}
	function getVotes()
        {
                return $this->votes;
        }
        function setVotes($votes)
        {
                $this->votes = $votes;
        }

	function getGenre()
	{
		return $this->genre;
	}

	function setGenre($genre)
	{
		$this->genre = $genre;
	}

}
